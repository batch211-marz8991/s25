// console.log("Hello World!")

//JSON Objects

/*
    JSON stands for Javascript Object Notation
    -Use also in other programming languages
    -Javascript objects are not to be confused with JSON
    -Serialization is  the process of converting data nito a series of bytes for easier transmission/transfer of information 
    -Use double qoutes for proper names
    -Syntax:
        {
            "propertyA": "valueA",
            "propertyB": "valueB"
        }
*/
/*
    JSON objects

    "city": "Quezon City",
    "province": "Metro Manila",
    "country": Philippines


    JSON Arrays

    "cities": [
            {"city": "Quezon City", "province": "Metro Manila", "country": Philippines},
            {"city": "Quezon City", "province": "Metro Manila", "country": Philippines},
            {"city": "Quezon City", "province": "Metro Manila", "country": Philippines},
    ]
*/


//JSOn Methods



//Converting data into stringified JSON

let batchesArr = [{batchName: 'batchX'}, {batchName: 'batchY'}]

//the "stringify method are use to convert javascript object into string"

console.log("Result to stringified method");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
    name: 'John',
    age: 31,
    address: {
        city: 'Manila',
        country: 'Philippines'
    }
})
console.log(data);

//using stringify method with variables

//syntax:
//      JSON.stringify({
//    propertyA: variableA,
//    propertyB: variableB
//  })
//eser data

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
    city: prompt('Which city do you live in?'),
    country: prompt('Which country does your city address belong to?')
};

let otherData = JSON.stringify({
    firstName: firstName,
    lastName: lastName,
    age: age,
    address: address
})
console.log(otherData);



//converting stringify JSOn into javascript objects
/*
    -Object are common data type used in applications because of the complex data structure that can be created out of them
    - information is commonly sent to application in stringified JSOn and then converted back into objects
    -this happens both for 

*/
let batchesJSON = `[{"batchName": "BatchX"}, {"batchName": "BatchY"}]`;
console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{"name": "John", "age": "31", "address": { "city": "Manila", "country": "Philppines"}}`

console.log(JSON.parse(stringifiedObject));


